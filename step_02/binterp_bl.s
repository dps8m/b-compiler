" This code goes at the front of the linked output

" Opcodes

a = 040000
b = a+a
c = b+a
f = c+a
n = f+a
s = n+a
t = s+a
u = t+a
x = u+a
y = x+a

start:
	s	2
	x	rtinit
	n	1 	" mcall
	s	2
	x	.main
	n	1	" mcall
	n	7	" retrn
	
	
	
rtinit:	.+1
	s	2	
	n	8	" escp
	n	7	" retrn
	5	" The escape code for rtinit
.argc		" the address of argc
.argv		" the address of argv

" the pdp7 seems to be using 0 for object output and 1 for error msgs
.fout:	0
.fin:	0
	
.flush:	.+1
	s	2	
	n	8	" escp
	n	7	" retrn
	2		" The escape code for flush
	
.write:	.+1
	s	2	
	n	8	" escp
	n	7	" retrn
	1		" The escape code for write
.fout		" The address of the fd
	
.read:	.+1
	s	2	
	n	8	" escp
	n	7	" retrn
	0		" The escape code for read
.fin		" The address of the fd
	
" .array  ????
	
.getchar:	.+1
	s	2
	n	8	" escp
	n	7	" retrn
	3		" The escape code for getchar
	
.putchar:	.+1
	s	2
	n	8	" escp
	n	7	" retrn
	4		" The escape code for putchar
	
.argc:	0

.argv:	.+1
.+1
.=.+10
	
