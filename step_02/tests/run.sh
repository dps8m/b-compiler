#!/bin/bash

BC7='../../step_01/tools_b'
AS7='perl ../../pdp7-unix-stash/as7'
EX7='perl ../../pdp7-unix-stash/a7out'


run () {
  echo $1
  $BC7  $1.b  $1.s
  $AS7 --out $1.out ../../pdp7-unix-stash/bl.s $1.s ../../pdp7-unix-stash/bi.s 2>$1.debug
  cp $1.data data
  $EX7 $1.out data list
  mv list $1.list
  $AS7 --out $1.out.new ../unix_bl.s $1.s ../unix_bi.s 2>>$1.debug
  ../binterp $1.out.new < $1.data >$1.list.new 2>>$1.debug || echo "binterp error"
  cmp -s $1.list $1.list.new || diff $1.list $1.list.new
}

run t0001
run t0002
run t0003
run t0004
echo Done.

