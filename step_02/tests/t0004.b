
putnum(n, b) {
  auto a, d;
   
  d = 0;
  if (n < 0) {
    n = -n;
    if (n < 0) {
      n = n-1;
      d = 1;
    } else
      write('-');
  }
  if (a = n/b)
    putnum(a, b);
  write(n%b + '0' + d);
}

putdig(d) {
  write('0' + d);
}

test(ans,que) {
  write ('t');
  putdig (ans);
  putdig (que);
  write ('*n');
}

main() {
  auto i;
  write('a*n');
  write ('b');
  write ('*n');
  i = 0;
  write (i + '0');
  i = 1;
  write (i + '0');
  i = 2;
  write (i + '0');
  i = 3;
  write (i + '0');
  write ('*n');

  i = 0;
  putdig (i);
  i = 1;
  putdig (i);
  i = 2;
  putdig (i);
  i = 3;
  putdig (i);
  write ('*n');

  test (1, 1);
  test (1, 0<1);
  test (0, 1<0);
  test (0, 0>1);
  test (1, 1>0);
  test (1, 0<=1);
  test (1, 1<=1);
  test (0, 2<=1);
  test (0, 0>=1);
  test (1, 1>=1);
  test (1, 2>=1);

  write ('+ '); test (2, 1 + 1);
  write ('- '); test (2, 4 - 2);
  write ('** '); test (8, 4 * 2);
  write ('/ '); test (2, 4 / 2);
  write ('% '); test (1, 5 % 2);

  test (2, ((-2) + 4));
  test (1, -1<1);
  test (0, 1<-1);
  test (0, -1>1);
  test (1, 1>-1);

  test (0, 1<=0);
  test (1, 0<=0);
  test (1, -1<=0);
  test (1, 1>=0);
  test (1, 0>=0);
  test (0, -1>=0);

  i = 0;
  while (i < 10) { 
    write (i + '0');
    i = i + 1;
  }
  write ('*n');

  putnum(1234);
  }

