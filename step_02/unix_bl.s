   x .main
   n 1

.fout: 1
.flush: .+1
   n 8
   n 7
   2  " The escape code for flush
   "jms flush
   "jmp fetch
.write: .+1
   s 2 
   n 8 
   n 7 
   1  " The escape code for write
   "lac sp i
   "dac t1
   "lrss 9
   "jms putc
   "lac t1
   "jms putc
   "jmp fetch
.read: .+1
   s 2 
   n 8 
   n 7 
   0  " The escape code for read
   "lac sp
   "tad d1
   "dac sp i
   "isz sp
   "jms getc
   "dac sp i
   "isz sp
   "jmp fetch



