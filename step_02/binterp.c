/* Interpret pdp7 B assembler output

  010000: 000001  .fout: 1
  010001: 010002  .flush: .+1
  010002: 240010     n 8
  010003: 240007     n 7
  010004: 010005  .write: .+1
  010005: 010006  .read: .+1
  010006: 300002     s 2 
  010007: 240010     n 8 
  010010: 240007     n 7 
  010011: 010012  .main:.+1
*/

#include <stdio.h>
#include <string.h>
#ifdef ansi
#include <stdlib.h>
#else
char *strdup(src)
  char *src;
  {
    char *str;
    char *p;
    int len = 0;
    char * malloc ();

    while (src[len])
        len++;
    str = malloc(len + 1);
    p = str;
    while (*src)
        *p++ = *src++;
    *p = '\0';
    return str;
  }
#endif

#define name_chars "abcdefghijklmnopqrstuvwxyz" \
                   "ABCDEFGHIJKLMNOPQRSTUVWXYZ" \
                   "0123456789."

#define wmask 0777777
#define signbit  0400000

/*#define memory_size (1 << 18)*/
#define memory_size ((1 << 16)-1)
static unsigned int memory[memory_size];
static char * memory_annotation[memory_size];

static unsigned int low, high;

static unsigned int pc;
static unsigned int running;
static unsigned int sp, dp, ap, sp_base;
static unsigned int t1, t2, t3, t4;
static unsigned int i8, i9;
static unsigned int ac, mq;
static unsigned int escape_code;
static unsigned int error_status;

/*
   Stack frames
  
   Note: Stack grows up.
         SP points to top of stack + 1
   
   Initial state
  
  
      stack, sp, dp, ap:  ---------------- 
  
  
   The runtime library starts main with:
  
      x .main  ; extern
      n 1      ; mcall (call with no arguments)
  
   The 'x' pushes the address and an empty word
  
         sp:
              ???
      stack:  .main           
  
   'mcall' sets up a stack frame with no arguments
  
   DP is saved on the stack
  
         sp:
              ???
      stack:  saved dp        
  
   DP is set to point to the saved dp
   
             sp:
                  ???
      stack, dp:  saved dp        
  
   PC is saved on the stack
  
             sp:
                  saved pc
      stack, dp:  saved dp        
  
   The PC is set to *.main (less 1 because the VM uses a pre-increment PC)
  
  
   When .main is entered, is allocates space for locals plus two spaces for the saved PC and DP
  
      .main:.+1
      s 2  (setop)
  
   In this example ("main () {}"), no locals so the setop operand is 2 (saved PC and DP).
   setop sets the SP to DP + the setop operand.
  
             sp:
                  saved pc
      stack, dp:  saved dp        
      
  
  
   For a call with arguments:
  
     main $(
      write('He');
     $)
  
    x .write
    n 2   (mark)
    n 5   (litrl)
    36965
    n 3   (call)
    s 2
  
  
   'x .write' pushes .write and an empty word
  
             sp:
                  ???
      stack, dp:  .write           
  
    'mark' saves *.write on the stack
  
             sp:
                  *.write
      stack, dp:  .write           
  
    Saves the ap on the stack
  
             sp:
                  saved ap
      stack, dp:  .write           
  
    And sets the AP to point to the stack second word
  
                 sp:
                      saved ap
      stack, ap, dp:  .write           
  
   LITRL pushes the address of the stack position where the literal value 
   will be pushed, and then pushes the literal value
  
                 sp:
                      value
                      &value on stack
                      saved ap
      stack, ap, dp:  .write           
  
   CALL converts the argments (from ap + 2 to TOS) from addr/value pairs to value.
  
       from <= ap + 1
       to <= ap + 1
       do {
             t1 = * ++from // points to an addr
             *to = *t1     // put *addr on the stack
             ++ from       // skip over value
          }
       while (sp - 1 != from)
  
                 sp:
                      value
                      value
                      saved ap
      stack, ap, dp:  .write           
  
  
   CALL then saves DP and PC on the stack
  
                 sp:
                      value
                      value
                      saved pc
      stack, ap, dp:  saved dp           
  
    Sets DP to AP
    Sets AP to the entry point address
       ap: .write
    Sets PC to entry point address -1 and calls the entry point.
  
    When the call returns, a "s 2" is executed to reset the SP, discarding
    the argument list.
  
 */ 

static int debug = 0;

static char * format_instruction (instr, buf)

unsigned int instr;
char * buf;
  {
    unsigned int opcode = (instr >> 14) & 017;
    unsigned int addr = instr & 017777;

    switch (opcode)
      {
        case 1: sprintf (buf, "autop %u", addr); break;
        case 2: switch (addr) {
          case 1: sprintf (buf, "b ="); break;
          case 2: sprintf (buf, "b |"); break;
          case 3: sprintf (buf, "b &"); break;
          case 4: sprintf (buf, "b =="); break;
          case 5: sprintf (buf, "b !="); break;
          case 6: sprintf (buf, "b <="); break;
          case 7: sprintf (buf, "b <"); break;
          case 8: sprintf (buf, "b >="); break;
          case 9: sprintf (buf, "b >"); break;
          case 12: sprintf (buf, "b +"); break;
          case 13: sprintf (buf, "b -"); break;
          case 14: sprintf (buf, "b %%"); break;
          case 15: sprintf (buf, "b *"); break;
          case 16: sprintf (buf, "b /"); break;
          default: sprintf (buf, "binary ???"); break;
        } break;
        case 3: sprintf (buf, "consop %u", addr); break;
        case 4: sprintf (buf, "ifop %u", addr); break;
        case 5: switch (addr) {
          case 1: sprintf (buf, "mcall"); break;
          case 2: sprintf (buf, "mark"); break;
          case 3: sprintf (buf, "call"); break;
          case 4: sprintf (buf, "vector"); break;
          case 5: sprintf (buf, "litrl"); break;
          case 6: sprintf (buf, "goto"); break;
          case 7: sprintf (buf, "retrn"); break;
          case 8: sprintf (buf, "escp"); break;
          default: sprintf (buf, "etcop ???"); break;
        } break;
        case 6: sprintf (buf, "setop %u", addr); break;
        case 7: sprintf (buf, "traop %u", addr); break;
        case 8: switch (addr) {
          case 1: sprintf (buf, "u &"); break;
          case 2: sprintf (buf, "u -"); break;
          case 3: sprintf (buf, "u *"); break;
          case 4: sprintf (buf, "u !"); break;
          default: sprintf (buf, "unary ???"); break;
        } break;
        case 9: sprintf (buf, "extern %u", addr); break;
        case 10: sprintf (buf, "array %u", addr); break;
      default: sprintf (buf, "???"); break;
    }
    return buf;
  }

static int usage ()
  {
    fprintf (stderr, "Usage: binterp [-d] foo.out\n");
    exit (1);
  }

int main (argc, argv)
int argc;
char * argv [];
  {
    FILE * infile = NULL;
    int rc, i, j, n;
    unsigned int addr, value;
    unsigned int instr, opcode;
    int ch;
    char inbuf [81];
    char * p, * q;
    char buf[256];

    unsigned int fout_addr;
    unsigned int fout;
    unsigned int fin_addr;
    unsigned int fin;

    unsigned char c0, c1;

    unsigned int foo1, foo2;

    unsigned int last, argc_addr, argv_addr, argl, arglw;

    unsigned int argp = 1;
    argc --;

    if (argc < 1)
      usage ();

    if (strcmp (argv[argp], "-d") == 0)
      {
        debug = 1;
        argp ++;
        argc --;
        if (argc < 1)
          usage ();
      }

    infile = fopen (argv[argp], "r");
    if (! infile)
      {
        fprintf (stderr, "Can't open %s\n", argv [argp]);
        exit (1);
      }

    memset (memory, 0, sizeof (memory));
    memset (memory_annotation, 0, sizeof (memory_annotation));
    low = memory_size;
    high = 0;

    while (! feof (infile))
      {
        p = fgets (inbuf, sizeof (inbuf), infile);
        if (! p)
          break;

        /* discard trailing NLs */
        q = p + strlen (p) - 1;
        while (p <= q && * q == '\n')
           * q -- = 0;

        if ((* p) == 0) /* empty line */
          continue;

        rc = sscanf (inbuf, "%06o: %06o", & addr, & value);
        if (rc != 2)
          {
            fprintf (stderr, "bail on parsing <%s>\n", inbuf);
            continue;
          }
        if (addr >= memory_size)
          {
            fprintf (stderr, "addr out of range <%s>\n", inbuf);
            continue;
          }

        memory[addr] = value;
        memory_annotation[addr] = strdup (inbuf + 15);

        if (addr < low)
          low = addr;
        if (addr > high)
          high = addr;
      } 

#if 0
    for (i = low; i <= high; i ++)
      printf ("%06o: %06o\t%s\n", i, memory[i], memory_annotation[i] ? memory_annotation[i] : "");
#endif

    pc = 010000 - 1;
    sp_base = dp = ap = sp = high;

    running = 1;
    error_status = 0;

    while (running)
      {
        if (pc >= memory_size)
          {
            fprintf (stderr, "pc out of range %o\n", pc);
            error_status = 1;
            break;
          }

        pc ++;
        instr = memory[pc];
        if (debug)
          {
            fprintf (stderr, "%06o:%06o %s   ; %s\n", pc, instr, format_instruction (instr, buf), memory_annotation[pc]);
          }
        opcode = (instr >> 14) & 017;
        addr = instr & 017777;
        switch (opcode)
          {

/*
   autop addr
  
    push  dp + addr
*/

            case 1: /* 04 a autop */
              /* autop:
                    lac addr
                    tad dp
                    dac sp i
                    isz sp
                    isz sp
                    jmp fetch */
              memory[sp] = (addr + dp) & wmask;
              sp = (sp + 2) & wmask;
              break;

            case 2: /* 10 b binop  */

              /* stack:  a_addr a_value b_addr b_value
                 binop:
                    -2
                    tad sp
                    dac sp */
              sp = (sp - 2) & wmask;    /* sp -> b_addr */

              /*    tad dm1 
                    dac t4 */
              t4 = (sp - 1) & wmask;    /* t4 -> a_value */

              /*    tad dm1
                    dac t3 */
              t3 = (t4 - 1) & wmask;    /* t3 -> a_addr */

              /*    lac t3 i
                    dac t1 */
              t1 = memory[t3];          /* t1 = a_addr */

              /*    lac sp i
                    dac t2 */
              t2 = memory[sp];          /* t2 = b_addr */

              /*    lac t4 
                    dac t3 i */
              memory[t3] = t4;          /* overwrite a_addr with & a_value */

              /*    lac addr
                    tad .+3
                    dac .+1
                    jmp .. i
                    jmp . i
                    basg; bor; band; beq; bne; ble; blt; bge; bgt; brsh; blsh
                    badd; bmin; bmod; bmul; bdiv */
              switch (addr)
                {
                  case 1:
                    /* basg:
                          lac t2 i
                          dac t1 i
                          dac t4 i */
                    memory[t4] = memory[t1] = memory[t2];   /* a_value = *a_addr = *b_addr */
                    /*    jmp fetch */
                    break;

                  case 2:
                    /* bor:
                          lac t1 i
                          lmq */
                    mq = memory[t1];   /* mq = *a_addr */

                    /*    lac t2 i
                          omq
                          dac t4 i */
                    memory[t4] = memory [t2] | mq;    /* a_value = *b_addr | * a_addr */

                    /*    jmp fetch */
                    break;

                  case 3:
                    /* band:
                          lac t1 i
                          and t2 i
                          dac t4 i */
                    memory[t4] = memory[t1] & memory[t2]; /* a_value = *b_addr | *a_addr */

                    /*    jmp fetch */
                    break;

                  case 4:
                    /* beq:
                          lac t1 i
                          xor t2 i */
                    ac = memory[t1] ^ memory[t2]; /* ac = *b_addr ^ *a_addr */

                    /*    sna cla
                          lac d1 */
                    if (ac == 0)
                      ac = 1;
                    else
                      ac = 0;

                    /*    dac t4 i */
                    memory[t4] = ac;  /* a_value = ! *b_addr ^ *a_addr */

                    /*    jmp fetch */
                    break;

                  case 5:
                    /* bne:
                          lac t1 i
                          xor t2 i */
                    ac = memory[t1] ^ memory[t2];

                    /*    sza
                          lac d1 */
                    if (ac != 0)
                      ac = 1;

                    /*    dac t4 i */
                    memory[t4] = ac;

                    /*    jmp fetch */
                    break;

                  case 6:
                    /* 
                       ble:
                          lac t2 i
                          cma */
                    ac = (~ memory[t2]) & wmask;  /* ac = ~ *b_addr */

                    /*    tad t1 i */
                    ac = (ac + memory[t1]) & wmask;       /* ac = ac  */

                    /*    spa cla
                          lac d1 */
                    if (ac & signbit)
                       ac = 1;
                    else
                       ac = 0;

                    /*    dac t4 i */
                    memory[t4] = ac;

                    /*    jmp fetch */
                    break;

                  case 7:
                    /* 
                       blt:
                          lac t1 i
                          cma
                          tad t2 i
                          sma cla
                          lac d1
                          dac t4 i
                          jmp fetch */
                    ac = (~ memory[t1]) & wmask;
                    ac = (ac + memory[t2]) & wmask;
                    if ((ac & signbit) == 0)
                      ac = 1;
                    else
                      ac = 0;
                    memory[t4] = ac;
                    break;

                  case 8:
                    /* 
                       bge:
                          lac t1 i
                          cma
                          tad t2 i
                          spa cla
                          lac d1
                          dac t4 i
                          jmp fetch */
                    ac = (~ memory[t1]) & wmask;
                    ac = (ac + memory[t2]) & wmask;
                    if (ac & signbit)
                      ac = 1;
                    else
                      ac = 0;
                    memory[t4] = ac;
		break;

	        case 9:
                    /* 
                       bgt:
                          lac t2 i
                          cma
                          tad t1 i
                          sma cla
                          lac d1
                          dac t4 i
                          jmp fetch */
                    ac = (~ memory[t2]) & wmask;
                    ac = (ac + memory[t1]) & wmask;
                    if ((ac & signbit) == 0)
                      ac = 1;
                    else
                      ac = 0;
                    memory[t4] = ac;
                    break;

                  /*case 10: */
                  /*case 11: */
                    /* 
                       brsh:
                       blsh:
                          hlt
                      break; */

                  case 12:
                    /* 
                       badd:
                          lac t1 i
                          tad t2 i
                          dac t4 i
                          jmp fetch */
                    memory[t4] = (memory[t1] + memory[t2]) & wmask;
                    break;

                  case 13:
                    /* 
                       bmin:
                          lac t1 i
                          cma
                          tad t2 i
                          cma
                          dac t4 i
                          jmp fetch */
                    /*memory[t4] = ((~ memory[t1]) + (~ memory[t2])) & wmask; */
                    memory[t4] = (memory[t1] - memory[t2]) & wmask;
                    break;

                  case 14:
                    /* 
                       bmod:
                          lac t2 i
                          dac .+4
                          lac t1 i
                          cll; idiv; ..
                          dac t4 i
                          jmp fetch */

/* The Mulitcs C compiler generates flaky code here; do the modulus */
/* the hard way */
                    /* memory[t4] = (memory[t1] % memory[t2]) & wmask; */
                    foo1 = memory[t1] / memory[t2];
                    foo1 = foo1 * memory[t2];
                    memory[t4] = memory[t1] - foo1;
                    break;

                  case 15:
                    /* 
                       bmul:
                          lac t2 i
                          dac .+4
                          lac t1 i
                          cll; mul; ..
                          lacq
                          dac t4 i
                          jmp fetch */
                    memory[t4] = (memory[t1] * memory[t2]) & wmask;
                    break;

                  case 16:
                    /* 
                       bdiv:
                          lac t2 i
                          dac .+4
                          lac t1 i
                          cll; idiv; ..
                          lacq
                          dac t4 i
                          jmp fetch */
                    memory[t4] = (memory[t1] / memory[t2]) & wmask;
                    break;

                  default:
                    fprintf (stderr, "bad b addr %06o:%06o\n", pc, instr);
                    error_status = 2;
                    running = 0;
                    break;
                }
              break;

            case 3: /* 14 c consop */
              /* consop:
                    lac sp
                    tad d1 /1
                    dac sp i */
              memory[sp] = (sp + 1) & wmask;

              /*    isz sp */
              sp ++;

              /*    lac addr
                    dac sp i */
              memory[sp] = addr;

              /*    isz sp */
              sp ++;

              /*    jmp fetch */
              break;

            case 4: /* 20 f ifop */
              /* ifop:
                    -2
                    tad sp
                    dac sp */
              sp = (sp - 2) & wmask;
              /*    lac sp i
                    dac t1 */
              t1 = memory[sp];
              /*    lac t1 i
                    sza 
                    jmp fetch */
              if (memory[t1])
                 break;
              /*    -1
                    tad addr i
                    dac pc */
              pc = (memory[addr] - 1) & wmask;
              /*    jmp fetch */
              break;

            case 5: /* 24 n etcop */
              switch (addr)
                {

/*
   n 1 mcall (call with no arguments)
*/

                  case 1: /* mcall */

                    /* stack: addr unknown
                       
                       mcall:
                          -2
                          tad sp
                          dac t1 */
                    t1 = (sp - 2) & wmask;  /* t1 points at addr */

                    /*    lac t1 i
                          dac t2       t2 = * t1 */
                    t2 = memory[t1];        /* t2 = addr */

                    /*    -1          ac = -1
                          tad t2 i    ac = ac + * t2
                          lmq         mq = ac */
                    mq =  (memory[t2] - 1) & wmask;  /* mq = (*addr) - 1 */

                    /*    lac dp      ac = dp
                          dac t1 i    * t1 = dp */
                    memory[t1] = dp;      /* stack: dp unknown */

                    /*    lac t1
                          dac dp */
                    dp = t1;              /* dp = & pushed dp */

                    /*    isz t1 */
                    t1 = (t1 + 1) & wmask;   /* t1 points to TOS */

                    /*    lac pc
                          dac t1 i */
                    memory[t1] = pc;      /* stack: dp pc */

                    /*    lacq
                          dac pc */
                    pc = mq;            /* pc = (* addr) - 1 */

                    /*    jmp fetch */
                    if (debug)
                      fprintf (stderr, "    mcall\n");
                    break;

/*
   n 2 mark
  
    in:
      stack: addr unknown
    out:
      stack: saved_ap *addr
      ap:    &second
*/

                  case 2: /* mark */


                    /* mark:
                          -1
                          tad sp
                          dac t2 */
                    t2 = (sp - 1) & wmask;   /* t2 <= & TOS */

                    /*    tad dm1
                          dac t1 */
                    t1 = (t2 - 1) & wmask;   /* t1 <= & second */

                    /*    lac t1 i
                          dac t3 */
                    t3 = memory[t1];         /* t3 <= second */

                    /*    lac t3 i
                          dac t2 i */
                    memory[t2] = memory[t3];  /* TOS <= * second */

                    /*    lac ap
                          dac t1 i */
                    memory[t1] = ap;         /* second <= ap */

                    /*    lac t1
                          dac ap */
                    ap = t1;                 /* ap <= & second */

                    /*    jmp fetch */
                    break;

/*
   n 3 call
*/

                  case 3: /* call */
                    {
                      /* call:
                            lac ap
                            tad d1
                            dac 8 */
                      i8 = (ap + 1) % wmask;   /* i8 <= ap + 1 */

                      /*    dac 9 */
                      i9 = i8;                 /* i9 <= ap + 1 */

                      /* 1: */
call1:
                      /*    lac 8 i
                            dac t1 */
                      i8 = (i8 + 1) & wmask;  /* i8 ++ */
                      t1 = memory[i8];        /* t1 <= *i8  (ap[2], ap[4], ...) */

                      /*    lac t1 i
                            dac 9 i */
                      i9 = (i9 + 1) & wmask;   /* i9 ++ */
                      memory[i9] = memory[t1]; /* *ip <= *t1
                                                  *ap[2] = **ap[2],
                                                  *ap[3] = **ap[3], */

                      /*    isz 8 */
                      i8 = (i8 + 1) & wmask;  /* i8 ++ */

                      /*    -1
                            tad sp */
                      ac = (sp - 1) % wmask;  /* ac = & TOS */

                      /*    sad 8
                            skp
                            jmp 1b */
                      if (ac != i8)          /* if &TOD != ap+3  (ap+5, ap+7, ....) */
                        goto call1;

                      /*    lac ap i
                            lmq */
                      mq = memory[ap];      /* mq = *ap  entry point address */

                      /*    lac dp
                            dac ap i */
                      memory[ap] = dp;      /* *ap = dp */

                      /*    lac ap
                            dac dp */
                      dp = ap;              /* dp = ap */

                      /*    isz ap */
                      ap = (ap + 1) & wmask;

                      /*    -1
                            tad ap i
                            dac t1 */
                      t1 = (memory[ap] - 1) & wmask;

                      /*    lac pc
                            dac ap i */
                      memory[ap] = pc;

                      /*    lacq
                            dac ap */
                      ap = mq;

                      /*    lac t1
                            dac pc */
                      pc = t1;

                      /*    jmp fetch */
                    }
                    break;

                  case 4: /* vector */

                    /* vector:
                          -2
                          tad sp
                          dac sp */
                    sp = (sp - 2) & wmask;

                    /*    tad dm2
                          dac t1 */
                    t1 = (sp - 2) & wmask;

                    /*    lac sp i
                          dac t2 */
                    t2 = memory[sp];

                    /*    lac t1 i
                          dac t3 */
                    t3 = memory[t1];

                    /*    lac t3 i
                          tad t2 i
                          dac t1 i */
                    memory[t1] = (memory[t2] + memory[t3]) & wmask;

                    /*    jmp fetch */
                    break;

/*
     n 5 litrl
  
     value = *pc++
     push  addr(value) value
*/

                  case 5: /* litrl */

                    /* litrl:
                          lac sp
                          tad d1
                          dac sp i */
                    memory[sp] = (sp + 1) & wmask; /* *(TOS+1) <= &TOS + 2 */

                    /*    isz sp */
                    sp = (sp + 1) & wmask;

                    /*    lac pc i
                          dac sp i */
                    pc = (pc + 1) & wmask;
                    memory[sp] = memory[pc];

                    /*    isz sp */
                    sp = (sp + 1) & wmask;

                    /*    jmp fetch */
                    break;

                  case 6: /* goto */
                    /* goto:
                          -2
                          tad sp
                          dac sp */
                    sp = (sp - 2) % wmask;
                    /*    lac sp i
                          dac t1 */
                    t1 = memory[sp];
                    /*    -1
                          tad t1 i
                          dac pc */
                    pc = (memory[t1] - 1) & wmask;

                    /*    jmp fetch */
                   break;

/*
   n 7 retrn
*/

                  case 7: /* retrn */
                    /* retrn:
                          -2
                          tad sp
                          dac sp */
                    sp = (sp - 2) % wmask;

                    /*    lac sp i
                          dac t1 */
                    t1 = memory[sp]; /* the return address */

                    /*    lac t1 i
                          lmq  */
                    mq = memory[t1];  /* the word at the return address? */

                    /*    lac dp
                          dac sp 
                          dac t1 */
                    t1 = sp = dp;

                    /*    lac sp i */
                    ac = memory[sp];   /* the saved dp */

                    /*    sna 
                          jmp stop */
                    //if (ac == 0)
                    if (ac == sp)
                      {
                        running = 0;
                        error_status = 0;
                      }

                    /*    dac dp */
                    dp = ac;

                    /*    isz sp */
                    sp = (sp + 1) & wmask;

                    /*    lac sp
                          dac t1 i */
                    memory[t1] = sp;

                    /*    lac sp i
                          dac pc */
                    pc = memory[sp];

                    /*    lacq
                          dac sp i */
                    memory[sp] = mq;

                    /*    isz sp */
                    sp = (sp + 1) & wmask;

                    /*    jmp fetch */
                    break;

/*
   n 8 escp
*/

                  case 8: /* escp  */
                    /* escp:
                          law 2
                          tad pc
                          dac t1
                          jmp t1 i */
                    escape_code = memory[(pc + 2) & wmask];
                    switch (escape_code)
                      {
                        /* Read should use fin; getchar should be stdin */
                        case 0: /* read */
                        case 3: /* getchar */
                          {
                            /* .read: .+1
                                  s 2
                                  n 8
                                  n 7
                                  lac sp
                                  tad d1
                                  dac sp i */
#if 0
                            /* Not using fin yet */
                            if (escape_code == 0) /* read */
                              {
                                fin_addr = memory[(pc + 3) & wmask];
                                fin = memory [fin_addr & wmask];
                              }
                            else  /* getchar */
                              fin = 0; /* stdin */
#endif

                            memory[sp] = (sp + 1) & wmask;
                            /*    isz sp */
                            sp = (sp + 1) & wmask;
                            /*    jms getc */
                            ch = fgetc (stdin);
                            if (ch == EOF)
                              ch = 4;
                            /*    dac sp i */
                            memory[sp] = ((unsigned int) ch) & wmask;
                            /*    isz sp */
                            sp = (sp + 1) & wmask;
                            /*    jmp fetch */
                          }
                          break;

                        /* Write should use fout; putchar should be stdout */
                        /* Write sends two bytes. putchar 1. */

                        case 4: /* putchar */
                          {
                            /* .write: .+1
                                  s 2 
                                  n 8 
                                  n 7
                                  lac sp i
                                  dac t1
                                  lrss 9
                                  jms putc
                                  lac t1 
                                  jms putc
                                  jmp fetch */

                            t1 = memory[sp];
                            c1 = (unsigned char) (t1 & 0xff);
                            if (c1)
                              fputc (c1, stdout);
                          }
                          break;

                        case 1: /* write */
                          {
                            /* .write: .+1
                                  s 2 
                                  n 8 
                                  n 7
                                  lac sp i
                                  dac t1
                                  lrss 9
                                  jms putc
                                  lac t1 
                                  jms putc
                                  jmp fetch */

                            /* fout usage seems to be
                             *   0 the object file
                             *   1 the console */
                            fout_addr = memory[(pc + 3) & wmask];
                            fout = memory [fout_addr & wmask];
                            t1 = memory[sp];
                            c0 = (unsigned char) ((t1 >> 9) & 0xff);
                            if (c0)
                              fputc (c0, fout ? stderr : stdout);
                            c1 = (unsigned char) (t1 & 0xff);
                            if (c1)
                              fputc (c1, fout ? stderr : stdout);
                          }
                          break;

                        case 2: /* flush */
                          fflush (stdout);
                          break;

                        case 5: /* rtinit */
                          /* copy args to high memory */
                          last = memory_size;
                          argc_addr = memory[(pc + 3) & wmask];
                          argv_addr = memory[memory[(pc + 4) & wmask]];
                          memory[argc_addr] = argc;
                          for (i = 0; i < argc; i ++)
                            {
                              argl = strlen (argv [argp + i]);
                              /* plus 1 for the terminating char. plus
                               * one to round up */
                              arglw = (argl + 2) / 2;
                              last -= arglw;  /* allocate space */
                              for (j = 0; j <= argl; j += 2)
                                {
                                  c0 = (unsigned char) argv[argp + i][j];
                                  if (j == argl)
                                    c1 = 4;
                                  else
                                    c1 = (unsigned char) argv[argp + i][j + 1];
                                  memory [last + j / 2] = c0 * 512u + c1;
                                }
                              memory[argv_addr + i] = last;
                            }
                          break;

                        default:
                          fprintf (stderr, "bad escape code %d %06o:%06o\n", escape_code, pc, instr);
                          error_status = 3;
                          running = 0;
                          break;
                      }
                    break;

                  default:
                    fprintf (stderr, "bad n addr %06o:%06o\n", pc, instr);
                    error_status = 4;
                    running = 0;
                    break;
                }
              break;

            case 6: /* 30 s setop   allocate stack space for locals */
              /* setop:
                    lac addr
                    tad dp
                    dac sp
                    jmp fetch */
              sp = (addr + dp) & wmask;
              if (debug)
                fprintf (stderr, "    setop %d\n", addr);
              break;

            case 7: /* 34 t traop */

              /* traop:
                    -1
                    tad addr
                    dac pc
                    jmp fetch */
              pc = (addr - 1) & wmask;
              break;

            case 8: /* 40 u unaop */
              /* unaop: 
                    -1
                    tad sp
                    dac t3 */
              t3 = (sp - 1) & wmask;   /* t3 points to TOS */

              /*    tad dm1  /-1
                    dac t2 */
              t2 = (t3 - 1) & wmask;   /* t2 points to second */

              /*    lac t2 i
                    dac t1  */
              t1 = memory[t2];         /* t1 = second */

              /*    lac t3 
                    dac t2 i */
              memory[t2] = t3;         /* second = & tos */

              /*    lac addr
                    tad .+3
                    dac .+1
                    jmp .. i
                    jmp . i
                    uadr; umin; uind; unot */
              switch (addr)
                {
                  case 1: /* uadr */
                    /* uadr:
                          lac t1
                          dac t3 i
                          jmp fetch */
                    memory[t3] = t1;    /* tos = second */
                    break;

                  case 2: /* umin */
                    /* umin:
                          -1
                          tad t1 i
                          cma
                          dac t3 i
                          jmp fetch */
                    memory[t3] = (- memory[t1]) & wmask;  /* tos = - second */
                    break;

                  case 3: /* uind */
                    /* uind:
                          lac t1 i
                          dac t2 i
                          jmp fetch */
                    memory[t2] = memory[t1];
                    break;

                  case 4: /* unot */
                    /* unot:
                          lac t1 i
                          sna cla */
                    if ((ac = memory [t1]) == 0)

                    /*    lac d1  /1 */
                       ac = 1;
                    else
                       ac = 0;

                    /*    dac t3 i */
                    memory[t3] = ac;
                    
                    /*    jmp fetch */
                    break;

                  default:
                    fprintf (stderr, "bad u addr %06o:%06o\n", pc, instr);
                    error_status = 5;
                    running = 0;
                    break;
                }
              break;

/*
   x addr
  
   push addr
   push empty word
*/

            case 9: /* 44 x extop */
              /* extop:
                    lac addr
                    dac sp i
                    isz sp
                    isz sp
                    jmp fetch */
              memory[sp] = addr;
              sp = (sp + 2) & wmask;
              if (debug)
                fprintf (stderr, "    extern %06o\n", addr);
              break;


            case 10: /* 50 y aryop */

              /* aryop:
                    lac addr
                    tad dp
                    dac t1
                    tad d1
                    dac t1 i
                    jmp fetch */
              ac = (addr + dp) & wmask;
              t1 = ac;
              ac = (ac + 1) & wmask;
              memory[t1] = ac;
              break;

              

            default:
              fprintf (stderr, "bad opcode %06o:%06o\n", pc, instr);
              error_status = 6;
              running = 0;
              break;
          } /* opcode */
        if (debug)
          {
            fprintf (stderr, "    base %o sp %6o  ap %6o dp %6o\n", sp_base, sp, ap , dp);
            fprintf (stderr, "    stack: ");
            n = sp - sp_base;
            if (n > 6)
              n = 6;
            for (i = 0; i < n; i ++)
              fprintf (stderr, "%7o", memory[sp - (n - 1 - i) - 1]);
            fprintf (stderr, "\n");
          }
      }
   /* This should be where main leaves it's return value ... */
   if (! error_status)
     error_status = memory [sp+1];
   return error_status;
  }


