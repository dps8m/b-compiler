
# PDP7 B --> GCOS B.

# In step_01:
# 
#   Bootstrap the B compiler for the C version of the compiler.
#     -- Build the C version of the PDP-7 B compiler
#     -- Build the B version of the PDP-7 B compiler with the C version of the
#        compiler
#     -- Verify the B version by using it to build itself and comparing the
#        output
# 
# In step_02:
# 
#    Build a byte code interpter in C that can run the B compiler;
#    verify interpter correctness by building the B compiler with it
#    and comparing the output.

all: s01 s02

s01:
	cd step_01 && ./build.sh

s02:
	cd step_02 && $(MAKE)
	cd step_02/tests && ./run.sh


