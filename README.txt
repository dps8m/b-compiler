PDP7 B --> GCOS B.

Build the PDP7 emaulator from simh 4.0; this code assumes that 'pdp7'
is in your serach path.

In step_01:

  Bootstrap the B compiler for the C version of the compiler.
    -- Build the C version of the PDP-7 B compiler
    -- Build the B version of the PDP-7 B compiler with the C version of the 
       compiler
    -- Verify the B version by using it to build itself and comparing the
       output

In step_02:

   Build a byte code interpter in C that can run the B compiler; 
   verify interpter correctness by building the B compiler with it
   and comparing the output.


In step_03:

   Modifying the B compiler.
     -- Don't generate local label references; they are not well 
        supported by most assemblers.
 
   Evolve towards GCOS dialect.
     -- Add support for 'manifest'; fix compiler source to use
        manifest to improve readibility and maintainability.




