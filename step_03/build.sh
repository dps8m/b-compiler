#!/bin/bash

set -e

BC7='../step_01/tools_b'
AS7='perl ../pdp7-unix-stash/as7'
EX7='perl ../pdp7-unix-stash/a7out'
BINTERP='../step_02/binterp'

echo "Preprocessing b.b --> b.p..."
cpp -C -P -traditional b.b b.p

echo "Cross compiling b.p --> b.s..."
$BINTERP ../step_02/b_tools_b.out <b.p  >b.s 

echo "Cross assembling b.s --> b.out..."
$AS7 --out b.out ../step_02/binterp_bl.s b.s ../step_02/binterp_bi.s 

echo "Self compiling  [b.out] b.p --> b2.s..."
$BINTERP b.out <b.p >b2.s

echo "Self assembling b2.s --> b2.out..."
$AS7 --out b2.out ../step_02/binterp_bl.s b2.s ../step_02/binterp_bi.s 

echo "Compiling reference compiler [b2.out] ref.b --> ref.s..."
$BINTERP b2.out <../step_02/b.b >ref.s

echo "Assembling reference compiler ref.s --> ref.out..."
$AS7 --out ref.out ../step_02/binterp_bl.s ref.s ../step_02/binterp_bi.s 

echo "Compiling reference compiler with new reference compiler [ref] ref.b --> ref2.b..."
$BINTERP ref.out <../step_02/b.b >ref2.s

echo "Assembling reference compiler ref2.s --> ref2.out..."
$AS7 --out ref2.out ../step_02/binterp_bl.s ref2.s ../step_02/binterp_bi.s 

echo "Verifying  ref.out ref2.out..."
# trim annotations
cut -f 1 <ref.out >ref.out.cut
cut -f 1 <ref2.out >ref2.out.cut
diff -s ref.out.cut ref2.out.cut && echo Success

