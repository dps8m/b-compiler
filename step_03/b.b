/* b.b - B compiler for PDP-7 Unix

   Coding style and organization based on lastc1120c.c
  
   (C) 2016 Robert Swierczek, GPL3
  
*/

/* Notes:
 *
 * Not implented:
 *   ++ -- << >> 
 *   =| =& === =!= =< =<= => =>= =<< =>> =+ == =% =* =/
 *   switch
 *
 *  Undefined variables are not flagged.
 *
 */

#define NO_LOCAL_LABELS
#define FLAG_UNDEF

/* Character types */

#define CT_EOF   0    /* End of file */
#define CT_SMI   1    /* ; semicolon */
#define CT_LBR   2    /* { L. brace  */ /* or $( */
#define CT_RBR   3    /* } R. brace  */ /* or $) */
#define CT_LBK   4    /* [ L. brack  */
#define CT_RBK   5    /* ] R. brack  */
#define CT_LPN   6    /* ( L. paren  */
#define CT_RPN   7    /* ) R. paren  */
#define CT_COL   8    /* : colon     */
#define CT_CMA   9    /* , commma    */
#define CT_KW   19    /* KEYWORD     */
#define CT_NAM  20    /* NAME        */
#define CT_NUM  21    /* NUMBER      */
#define CT_EXC  34    /* ! bang      */
#define CT_PLS  40    /* + plus      */
#define CT_MIN  41    /* - minus     */
#define CT_MUL  42    /* * splat     */
#define CT_DIV  43    /* / slash     */
#define CT_PCT  44    /* % percent   */
#define CT_AMP  47    /* & amper.    */
#define CT_CRT  48    /* ^ caret     */ /* or vert. bar | */
#define CT_EEQ  60    /* == eq. eq.  */
#define CT_NEQ  61    /* != not eq.  */
#define CT_LE   62    /* <= le       */
#define CT_LT   63    /* < less than */
#define CT_EQ   80    /* = equals    */
#define CT_GE   64    /* <= ge       */
#define CT_GT   65    /* < gtr than  */
#define CT_QUE  90    /* ? question  */  /* Unused? */
#define CT_APO 121    /* ' apost.    */  /* Unused? */
#define CT_QUO 122    /* " quote     */
#define CT_LET 123    /* letter      */
#define CT_DIG 124    /* digit       */
#define CT_WHT 126    /* whitespace  */
#define CT_UNK 127    /* unknown     */

#define SYMTAB_SZ   300
#define SYMBUF_SZ    10
#define SYMBUF_LAST   9   /* SYMBUF_SZ - 1 */
#define SYMTAB_LAST 290   /* SYMTAB_SZ - SYMBUF_SZ */

/* symtab symbol table
 *
 *  class, value, char, char, ..., 0,
 *  class, value, char, char, ..., 0,
 *
 */

/* value field usage for class:
 *
 *   class        value field usage
 *   CL_UNK
 *   CL_KW        one of KW_*
 *   CL_INTERNAL  isn
 *
 */

#define CL_UNK      0
#define CL_KW       1
#define CL_INTERNAL 2
#define CL_AUTO     5
#define CL_EXTRN    6

#ifdef FLAG_UNDEF
#define CL_LABEL   99
#endif

/* Warning: extdef() relies on KW_AUTO and KW_EXTRN < KW_GOTO */

#define KW_AUTO 5
#define KW_EXTRN 6
#define KW_PARAM 8   /* arguments? */
#define KW_GOTO 10
#define KW_RETURN 11
#define KW_IF 12
#define KW_WHILE 13
#define KW_ELSE 14




/* storage */

/* symbol table */

/* inital values
 *
 *  class    value      name
 *
 *  CL_KW   KW_AUTO    auto
 *  CL_KW   KW_EXTRN   extrn
 *  CL_KW   KW_GOTO    goto
 *  CL_KW   KW_RETURN  return
 *  CL_KW   KW_IF      if
 *  CL_KW   KW_WHILE   while
 *  CL_KW   KW_ELSE    else
 *
 */

symtab[SYMTAB_SZ] /* class value name */
  CL_KW, KW_AUTO,   'a','u','t','o', 0 ,
  CL_KW, KW_EXTRN,  'e','x','t','r','n', 0 ,
  CL_KW, KW_GOTO,   'g','o','t','o', 0 ,
  CL_KW, KW_RETURN, 'r','e','t','u','r','n', 0 ,
  CL_KW, KW_IF,     'i','f', 0 ,
  CL_KW, KW_WHILE,  'w','h','i','l','e', 0 ,
  CL_KW, KW_ELSE,   'e','l','s','e', 0 ;

/* Warning: redefine if symtab changes */

#define SYMTAB_NEXT 51

ctab[]
  CT_EOF, CT_UNK, CT_UNK, CT_UNK, CT_EOF, CT_UNK, CT_UNK, CT_UNK,  /* NUL SOH STX ETX EOT ENQ ACK BEL */
  CT_UNK, CT_WHT, CT_WHT, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK,  /* BS  TAB LF  VT  FF  CR  SO  SI  */
  CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK,  /* DLE DC1 DC2 DC3 DC4 NAK SYN ETB */
  CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK, CT_UNK,  /* CAN EM  SUB ESC FS  GS  RS  US  */
  CT_WHT, CT_EXC, CT_QUO, CT_UNK, CT_UNK, CT_PCT, CT_AMP, CT_APO,  /* SPC  !   "   #   $   %   &   '  */
  CT_LPN, CT_RPN, CT_MUL, CT_PLS, CT_CMA, CT_MIN, CT_UNK, CT_DIV,  /*  (   )   *   +   ,   -   .   /  */
  CT_DIG, CT_DIG, CT_DIG, CT_DIG, CT_DIG, CT_DIG, CT_DIG, CT_DIG,  /*  0   1   2   3   4   5   6   7  */
  CT_DIG, CT_DIG, CT_COL, CT_SMI, CT_LT,  CT_EQ,  CT_GT,  CT_QUE,  /*  8   9   :   ;   <   =   >   ?  */
  CT_UNK, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET,  /*  @   A   B   C   D   E   F   G  */
  CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET,  /*  H   I   J   K   L   M   N   O  */
  CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET,  /*  P   Q   R   S   T   U   V   W  */
  CT_LET, CT_LET, CT_LET, CT_LBK, CT_UNK, CT_RBK, CT_CRT, CT_UNK,  /*  X   Y   Z   [   \   ]   ^   _  */
  CT_UNK, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET,  /*  `   a   b   c   d   e   f   g  */
  CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET,  /*  h   i   j   k   l   m   n   o  */
  CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET, CT_LET,  /*  p   q   r   s   t   u   v   w  */
  CT_LET, CT_LET, CT_LET, CT_LBR,  CT_CRT,CT_RBR, CT_UNK, CT_UNK;  /*  x   y   z   {   |   }   ~  DEL */

symbuf[SYMBUF_SZ];
peeksym -1;
peekc;
eof;
line 1;
csym;   /* the symbol returned by lookup() */
ns;

/* cval is the value field for cursym
 *   CT_NUM  the numeric value
 *   CT_KW   the value field from the symtab entry
 */

cval;
isn;
#ifdef NO_LOCAL_LABELS
isn1st;
gsn;
esn;  /* label of end of block */
#endif
nerror;
nauto;

/* Code generation */

#define OP_AUTOP   'a'
#define OP_BINARY  'b'
#define OP_CONST   'c'
#define OP_IF      'f'
#define OP_ETC     'n'
#define OP_TRA     't'
#define OP_UNARY   'u'

#define gen_autop(cval)  gen (OP_AUTOP, cval)

#define gen_asg()        gen (OP_BINARY, 1)
#define gen_bor()        gen (OP_BINARY, 2)
#define gen_band()       gen (OP_BINARY, 3)
#define gen_beq()        gen (OP_BINARY, 4)
#define gen_bne()        gen (OP_BINARY, 5)
#define gen_ble()        gen (OP_BINARY, 6)
#define gen_blt()        gen (OP_BINARY, 7)
#define gen_bge()        gen (OP_BINARY, 8)
#define gen_bgt()        gen (OP_BINARY, 9)
#define gen_badd()       gen (OP_BINARY, 12)
#define gen_bmin()       gen (OP_BINARY, 13)
#define gen_bmod()       gen (OP_BINARY, 14)
#define gen_bmul()       gen (OP_BINARY, 15)
#define gen_bdiv()       gen (OP_BINARY, 16)

#define gen_consop(cval) gen (OP_CONST, cval)

#define gen_ifop(cval)   gen (OP_IF, cval)

#define gen_mcall()      gen (OP_ETC, 1)
#define gen_mark()       gen (OP_ETC, 2)
#define gen_call()       gen (OP_ETC, 3)
#define gen_vector()     gen (OP_ETC, 4)
#define gen_literal()    gen (OP_ETC, 5)
#define gen_goto()       gen (OP_ETC, 6)
#define gen_retrn()      gen (OP_ETC, 7)

#define gen_setop(cval)  gen ('s', cval)

#define gen_uadr()       gen (OP_UNARY, 1)
#define gen_umin()       gen (OP_UNARY, 2)
#define gen_uind()       gen (OP_UNARY, 3)
#define gen_unot()       gen (OP_UNARY, 4)

#define gen_aryop(cval)  gen ('y', cval)


#define writenl() write('*n');


/**********
 *
 * main
 *
 */

main() {
  extrn symtab, eof, ns, nerror;
  while (!eof) {
    ns = symtab + SYMTAB_NEXT;
    extdef();
    blkend();
  }
  return(nerror != 0);
}

/**********
 *
 * lookup  -- search symtab for name in symbuf;
 *            if not found, add it
 *
 * Return pointer to symtab entry
 *
 */

lookup() {
  extrn symtab, symbuf, eof, ns;
  auto np, sp, rp;

  /* search the table for a name matching symbuf */

  rp = symtab;          /* rp points to the start of a record */
  while (rp < ns) {     /* until end of table */
    np = rp + 2;        /* np points to the name field */
    sp = symbuf;        /* sp points to the name being searched for */
    while (*np==*sp) {  /* strcmp */
      if (!*np)         /* if all chars matched to the NUL */
        return(rp);     /*   then return the pointer to the record */
      np = np+1;
      sp = sp+1;
    }

    while (*np)         /* names didn't match; search for the end of the name */
      np = np+1;
    rp = np+1;          /* set the rc to the start of the next record */
  }

  /* add symbuf to the table */

  sp = symbuf;

  /* enough room? */
  /* CAC: I am not sure about the correct value of SYMTAB_LAST */

  if (ns >= symtab + SYMTAB_LAST) {
    error('sf');
    eof = 1;
    return(rp);
  }

  *ns = CL_UNK;         /* initialize class to 0 */
  ns[1] = 0;            /* initialize value to 0 */
  ns = ns+2;            /* allocate class and value */
  while (*ns = *sp) {   /* copy symbuf to name field */
    ns = ns+1;
    sp = sp+1;
  }
  ns = ns+1;            /* advance past the name field end */

  /* return the pointer to the entry */

  return(rp);
}



/**********
 *
 *  symbol -- parse the next token
 *
 *  Return CT_*
 *  If CT_NUM, value is in cval
 *  If CT_KW or CT_NAM, name is in symbuf
 */

symbol() {
  extrn symbuf, ctab, peeksym, peekc, eof, line, csym, cval;
  auto b, c, ct, sp;

  /* if a symbol has been pushed back, pop it and return */

  if (peeksym>=0) {
    c = peeksym;
    peeksym = -1;
    return(c);
  }

  /* prime c with the next character in the input */

  /* if a character was pushed back pop it and use it */

  if (peekc) {
    c = peekc;
    peekc = 0;
  } else {

  /* get a character from the source file */

    if (eof)
      return(CT_EOF);
    c = read();
  }


loop:
  ct = ctab[c];
 
  if (ct == CT_EOF) { /* eof */
    eof = 1;
    return(CT_EOF);
  }
 
  if (ct == CT_WHT) { /* white space */
    if (c=='*n')
      line = line+1;
    c = read();
    goto loop;
  }

  if (c=='=')
    return(subseq('=',CT_EQ,CT_EEQ));

  if (c=='<')
    return(subseq('=',CT_LT,CT_LE));

  if (c=='>')
    return(subseq('=',CT_GT,CT_GE));

  if (c=='!')
    return(subseq('=',CT_EXC,CT_NEQ));

  if (c=='$') {
    if (subseq('(',0,1))
      return(CT_LBR);
    if (subseq(')',0,1))
      return(CT_RBR);
  }
  if (c=='/') {
    if (subseq('**',1,0))
      return(CT_DIV);

    /* Process comment */

com:
    c = read();
com1:
    if (c==4) {
      eof = 1;
      error('**/'); /* eof */
      return(CT_EOF);
    }
    if (c=='*n')
      line = line+1;
    if (c!='**')
      goto com;
    c = read();
    if (c!='/')
      goto com1;
    c = read();
    goto loop;
  }

  if (ct==CT_DIG) { /* number */
    cval = 0;
    if (c=='0')
      b = 8;
    else
      b = 10;
    while(c >= '0' & c <= '9') {
      cval = cval*b + c -'0';
      c = read();
    }
    peekc = c;
    return(CT_NUM);
  }

  if (c=='*'') { /* ' */
    getcc();
    return(CT_NUM);
  }

  if (ct==CT_LET) { /* letter */
    sp = symbuf;
    while(ct == CT_LET | ct == CT_DIG) {
      if (sp < symbuf + SYMBUF_LAST) {
        *sp = c;
        sp = sp+1;
      }
      ct = ctab[c = read()];
    }
    *sp = 0;
    peekc = c;
    csym = lookup();
    if (csym[0] == CL_KW) {
      cval = csym[1];
      return(CT_KW); /* keyword */
    }
    return(CT_NAM); /* name */
  }

  if (ct == CT_UNK) { /* unknown */
    error('sy');
    c = read();
    goto loop;
  }
  return(ctab[c]);
}



/**********
 *
 *  subseq  -- peek at the next character
 *    if it matches 'c', eat the characte and return 'b' else return 'a'
 *
 */

subseq(c,a,b) {
  extrn peekc;

  if (!peekc)
    peekc = read();
  if (peekc != c)
    return(a);
  peekc = 0;
  return(b);
}

/**********
 *
 *  getcc  get character constant ('.' or '..')
 *
 *  return value in cval
 *
 */

getcc() {
  extrn cval;
  auto c;

  /* '' evaluates to 0 */

  cval = 0;
  if ((c = mapch('*'')) < 0)
    return;

  /* 'c' evaluates to c */

  cval = c;
  if ((c = mapch('*'')) < 0)
    return;

  /* 'cd' evaluates to c << 9 + d */

  cval = cval * 512 + c;

  /* two characters max. */
  if (mapch('*'') >= 0)
    error('cc');
}

/**********
 *
 * getstr
 *
 */

getstr() {
  auto i, c, d;

  i = 1;
loop:
  if ((c = mapch('"')) < 0) {
    number(2048);
    writenl();
    return(i);
  }
  if ((d = mapch('"')) < 0) {
    number(c*512+4);
    writenl();
    return(i);
  }
  number(c*512+d);
  writenl();
  i = i+1;
  goto loop;
}

/**********
 *
 * mapch  -- get next character in string
 *           c is the delimiter
 *           process "*" escape sequences 
 *
 * Returns -1 if delimiter found or syntax error, else 
 * the unescaped character.
 *
 */

mapch(c) {
  extrn peekc;
  auto a;

  /* Get next character; done if delimiter */

  if ((a=read())==c)
    return(-1);
 
  /* Error if newline or EOF */

  if (a=='*n' | a==0 | a==4) {
    error('cc');
    peekc = a;
    return(-1);
  }

  /* Process escape sequence */

  if (a=='**') {
    a=read();

    if (a=='0')
      return(0);

    if (a=='e')
      return(4);

    if (a=='(')
      return('{');

    if (a==')')
      return('}');

    if (a=='t')
      return('*t');

    if (a=='r')
      return('*r');

    if (a=='n')
      return('*n');
  }
  return(a);
}

/**********
 *
 * expr
 *
 * lev is precedence
 *
 *    1   ! expr
 *    1   - expr
 *    1   & expr
 *    1   * expr
 *    1   ++ expr      (not implemented)
 *    1   -- expr      (not implemented)
 *    1   expr ++      (not implemented)
 *    1   expr --      (not implemented)
 *    2   ... * expr
 *    2   ... / expr
 *    2   ... % expr
 *    3   ... + expr
 *    3   ... - expr
 *    4   ... << expr  (not implemented)
 *    4   ... >> expr  (not implemented)
 *    5   ... <= expr
 *    5   ... < expr
 *    5   ... >= expr
 *    5   ... > expr
 *    6   ... == expr
 *    6   ... != expr
 *    7   ... & expr
 *    8                ?????
 *    9   ... | expr
 *   10   ... ? expr   (not implemented) (binds right to left)
 *   10   ... : expr   (not implemented) (binds right to left))
 *   14   ... = expr
 *   14      =| =& === =!= =< =<= => =>= =<< =>> =+ == =% =* =/ not implemented
 *   15   ( expr )
 *   15   [ expr ]
 *   15   goto expr 
 *   15   retrn expr 
 */

#define PREC_PRIMARY 15


expr(lev) {
  extrn peeksym, csym, cval, isn;
  auto o;
#ifdef NO_LOCAL_LABELS
  extrn gsn, esn, isn1st;
  auto g;
#endif

  o = symbol();
 
  if (o==CT_NUM) { /* number */
case21:

  /* If the value will fit into the addr field, use consop
   * else litrl */

    if ((cval & 017777)==cval) {
      gen_consop(cval);
      goto loop;
    }
    gen_literal();
    number(cval);
    writenl();
    goto loop;
  }

  if (o==CT_QUO) { /* string */

    /* x .+2     */
    /* t 2f      */  /* XXX local label */
    /* .+1       */
    /* <string>  */
    /* 2:        */

    write('x ');
    write('.+');
    write('2*n');

#ifdef NO_LOCAL_LABELS
    g = gsn;
    gsn = gsn + 1;
    write('t ');
    write('G');
    number(g);
    writenl();
#else
    write('t ');
    write('2f');   /* XXX local label */
    writenl();
#endif

    write('.+');
    write('1*n');

    getstr();

#ifdef NO_LOCAL_LABELS
    write('G');
    number(g);
    write(':*n');
#else
    write('2:');  /* XXX local label */
    writenl();
#endif
    goto loop; 
  }
  
  if (o==CT_NAM) { /* name */

    if (*csym==CL_UNK) { /* not seen */
      if ((peeksym=symbol())==CT_LPN) { /* ( */
        *csym = CL_EXTRN; /* extrn */
      } else {
        *csym = CL_INTERNAL; /* internal */
        csym[1] = isn;
        isn = isn+1;
      }
    }


    if (*csym==CL_AUTO) /* auto */
      gen_autop(csym[1]);
    else {
      write('x ');
      if (*csym==CL_EXTRN) { /* extrn */
        write('.');
        name(csym+2);
      } else { /* internal */
#ifdef NO_LOCAL_LABELS
        write('L');
        number(csym[1]);
#else
        write('1f');   /* XXX local label */
        write('+');
        number(csym[1]);
#endif
      }
      writenl();
    }
    goto loop;
  }
 
  if (o==34) { /* ! */
    expr(1);
    gen_unot();
    goto loop;
  }

  if (o==CT_MIN) { /* - */
    peeksym = symbol();
    if (peeksym==CT_NUM) { /* number */
      peeksym = -1;
      cval = -cval;
      goto case21;
    }
    expr(1);
    gen_umin();
    goto loop;
  }
 
  if (o==CT_AMP) { /* & */
    expr(1);
    gen_uadr();
    goto loop;
  }
 
  if (o==CT_MUL) { /* * */
    expr(1);
    gen_uind();
    goto loop;
  }
 
  if (o==CT_LPN) { /* ( */
    peeksym = o;
    pexpr();
    goto loop;
  }
  error('ex');

loop:
  o = symbol();

  if (lev>=14 & o==CT_EQ) { /* = */
    expr(14);
    gen_asg();
    goto loop;
  }
  if (lev>=10 & o==CT_CRT) { /* | ^ */
    expr(9);
    gen_bor();
    goto loop;
  }
  if (lev>=8 & o==CT_AMP) { /* & */
    expr(7);
    gen_band();
    goto loop;
  }

/* CAC: this is too clever to be maintainable */
#if 0
  if (lev>=7 & o>=CT_EEQ & o<=CT_NEQ) { /* == != */
    expr(6);
    gen('b',o-56); /* beq bne */
    goto loop;
  }
#else
  if (lev>=7 & o==CT_EEQ) { /* == */
    expr(6);
    gen_beq();
    goto loop;
  }
  if (lev>=7 & o==CT_NEQ) { /* != */
    expr(6);
    gen_bne();
    goto loop;
  }
#endif

/* CAC: this is too clever to be maintainable */
#if 0
  if (lev>=6 & o>=CT_LE & o<=CT_GT) { /* <= < >= > */
    expr(5);
    gen('b',o-56); /* ble blt bge bgt */
    goto loop;
  }
#else
/* use this instead */
  if (lev>=6 & o==CT_LE) { /* <= */
    expr(5);
    gen_ble();
    goto loop;
  }
  if (lev>=6 & o==CT_LT) { /* < */
    expr(5);
    gen_blt();
    goto loop;
  }
  if (lev>=6 & o==CT_GE) { /* >= */
    expr(5);
    gen_bge();
    goto loop;
  }
  if (lev>=6 & o==CT_GT) { /* r>*/
    expr(5);
    gen_bgt();
    goto loop;
  }
#endif

/* CAC: this is too clever to be maintainable */
#if 0
  if (lev>=4 & o>=CT_PLS & o<=CT_MIN) { /* + - */
    expr(3);
    gen('b',o-28); /* badd bmin */
    goto loop;
  }
#else
/* use this instead */
  if (lev>=4 & o==CT_PLS) { /* + */
    expr(3);
    gen_badd();
    goto loop;
  }
  if (lev>=4 & o==CT_MIN) { /* - */
    expr(3);
    gen_bmin();
    goto loop;
  }
#endif

/* CAC: this is too clever to be maintainable */
#if 0
  if (lev>=3 & o>=CT_MUL & o<=CT_DIV) { /* * / */
    expr(2);
    gen('b',o-27); /* bmul bdiv */
    goto loop;
  }
#else
  if (lev>=3 & o==CT_MUL) { /* * */
    expr(2);
    gen_bmul();
    goto loop;
  }
  if (lev>=3 & o==CT_DIV) { /* / */
    expr(2);
    gen_bdiv();
    goto loop;
  }
/* use this instead */
#endif

  if (lev>=3 & o==CT_PCT) { /* % */
    expr(2);
    gen_bmod();
    goto loop;
  }
  if (o==CT_LBK) { /* [ */
    expr(15);
    if (symbol() != CT_RBK)
      error('[]');
    gen_vector();
    goto loop;
  }
  if (o==CT_LPN) { /* ( */
    o = symbol();
    if (o==CT_RPN) /* ) */
      gen_mcall();
    else {
      gen_mark();
      peeksym = o;
      while (o!=CT_RPN) {
        expr(15);
        o = symbol();
        if (o!=CT_RPN & o!=CT_CMA) { /* ) , */
          error('ex');
          return;
        }
      }
      gen_call();
    }
    goto loop;
  }
 
  peeksym = o;
}

/**********
 *
 * pexpr -- parse   " ( <expr> ) "
 *
 */

pexpr() {
  if (symbol()==CT_LPN) { /* ( */
    expr(15);
    if (symbol()==CT_RPN) /* ) */
      return;
  }
  error('()');
}

/**********
 *
 * declare
 *
 *   parse
 *     auto <name_list>
 *     extrn <name_list>
 *     ( <argument_list>
 *
 *  kw is KW_AUTO, KW_EXTRN or KW_PARAM
 *
 */

declare(kw) {
  extrn csym, cval, nauto, error;
  auto o;

  while((o=symbol())==CT_NAM) { /* name */
    if (kw==KW_EXTRN) { /* extrn */
      *csym = CL_EXTRN;
      o = symbol();
    } else { /* auto/param */
      *csym = CL_AUTO; /* auto */
      csym[1] = nauto;
      o = symbol();
      if (kw==KW_AUTO & o==CT_NUM) { /* auto & number */
        gen_aryop(nauto);
        nauto = nauto + cval;
        o = symbol();
      }
      nauto = nauto+1;
    }
    if (o!=CT_CMA) /* , */
      goto done;
  }
done:
  if (o==CT_SMI & kw!=KW_PARAM | o==CT_RPN & kw==KW_PARAM) /* auto/extrn ;  param ')' */
    return;
syntax:
  error('[]'); /* declaration syntax */
}

/**********
 *
 * extdef
 *
 */

extdef() {
  extrn peeksym, csym, cval, nauto;
  auto o, c;

  o = symbol();
  if (o==0 | o==CT_SMI) /* eof ; */
    return;

  if (o!=CT_NAM) /* name */
    goto syntax;

  csym[0] = CL_EXTRN; /* extrn */
  write('.');
  name(csym + 2);
  write(':');
  o=symbol();
 
  if (o==CT_LBR | o==CT_LPN) { /* $( ( */
    write('.+');
    write('1*n');
    nauto = 2;
    if (o==CT_LPN) { /* ( */
      declare(KW_PARAM); /* param */
      if ((o=symbol())!=CT_LBR) /* $( */
        goto syntax;
    }
    while((o=symbol())==CT_KW & cval<KW_GOTO) /* auto extrn */
      declare(cval);
    peeksym = o;
    gen_setop(nauto); /* setop */
    stmtlist();
    gen_retrn();
    return;
  }

  if (o==CT_MIN) { /* - */
    if (symbol()!=CT_NUM) /* number */
      goto syntax;
    number(-cval);
    writenl();
    return;
  }

  if (o==CT_NUM) { /* number */
    number(cval);
    writenl();
    return;
  }

  if (o==CT_SMI) { /* ; */
    write('0*n');
    return;
  }

  if (o==CT_LBK) { /* [ */
    c = 0;
    if ((o=symbol())==CT_NUM) { /* number */
      c = cval;
      o = symbol();
    }
    if (o!=CT_RBK) /* ] */
      goto syntax;
    write('.+');
    write('1*n');
    if ((o=symbol())==CT_SMI) /* ; */
      goto done;
    while (o==CT_NUM | o==CT_MIN) { /* number - */
      if (o==CT_MIN) { /* - */
        if (symbol()!=CT_NUM)
          goto syntax;
        cval = -cval;
      }
      number(cval);
      writenl();
      c = c-1;
      if ((o=symbol())==CT_SMI) /* ; */
        goto done;
      if (o!=CT_CMA) /* , */
        goto syntax;
      else
        o = symbol();
    }
    goto syntax;
done:
    if (c>0) {
      write('.=');
      write('.+');
      number(c);
      writenl();
    }
    return;
  }

  if (o==0) /* eof */
    return;

syntax:
  error('xx');
  stmt();
}

/**********
 *
 * stmtlist
 *
 */

stmtlist() {
  extrn peeksym, eof;
  auto o;

  while (!eof) {
    if ((o = symbol())==CT_RBR) /* $) */
      return;
    peeksym = o;
    stmt();
  }
  error('$)'); /* missing $) */
}

/**********
 *
 * stmt
 *
 */

stmt() {
  extrn peeksym, peekc, csym, cval, isn, nauto;
  auto o, o1, o2;

next:
  o = symbol();

  if (o==0) { /* eof */
    error('fe'); /* Unexpected eof */
    return;
  }
 
  if (o==CT_SMI | o==CT_RBR) /* ; $) */
    return;
 
  if (o==CT_LBR) { /* $( */
    stmtlist();
    return;
  }

  if (o==CT_KW) { /* keyword */

    if (cval==KW_GOTO) { /* goto */
      expr(15);
      gen_goto();
      goto semi;
    }

    if (cval==KW_RETURN) { /* return */
      if ((peeksym=symbol())==CT_LPN) /* ( */
        pexpr();
      gen_retrn();
      goto semi;
    }

    if (cval==KW_IF) { /* if */
      pexpr();
      o1 = isn;
      isn = isn+1;
      jumpc(o1);
      stmt();
      o = symbol();
      if (o==CT_KW & cval==KW_ELSE) { /* else */
        o2 = isn;
        isn = isn+1;
        jump(o2);
        label(o1);
        stmt();
        label(o2);
        return;
      }
      peeksym = o;
      label(o1);
      return;
    }

    if (cval==KW_WHILE) { /* while */
      o1 = isn;
      isn = isn+1;
      label(o1);
      pexpr();
      o2 = isn;
      isn = isn+1;
      jumpc(o2);
      stmt();
      jump(o1);
      label(o2);
      return;
    }

    error('sx');
    goto syntax;
  }

  if (o==CT_NAM & peekc==':') { /* name : */
    peekc = 0;
#ifdef FLAG_UNDEF
    if (*csym == CL_UNK) {
      *csym = CL_LABEL;
      csym[1] = isn;
      isn = isn+1;
    } else if (*csym = CL_INTERNAL) {
      *csym = CL_LABEL;
    } else if (*csym != CL_LABEL) {
      errornm('rd', csym+2);
      goto next;
    }
#else
    if (!*csym) {
      *csym = CL_INTERNAL; /* param */
      csym[1] = isn;
      isn = isn+1;
    } else if (*csym != CL_INTERNAL) {
      error('rd');
      goto next;
    }
#endif
    label(csym[1]);
    goto next;
  }

  peeksym = o;
  expr(15);
  gen_setop(nauto);

semi:
  o = symbol();
  if (o==CT_SMI) /* ; */
    return;

syntax:
  error('sz');
  goto next;
}

dmpst() {
  extrn symtab, ns;
  auto rp;

  rp = symtab+SYMTAB_NEXT;
  while (rp < ns) {
    write ('" ');
    number(*rp);
    write(' ');
    number(*(rp+1));
    write(' ');
    name(rp+2);
    writenl();
    rp = rp+2;  /* move up to the name */
    while (*rp) /* move to the end of the name */
      rp = rp+1;
    rp = rp+1; /* move up to the start of the next entry */
  }
  writenl();
}

/**********
 *
 * blkend
 *
 */

blkend() {
  extrn isn;
  auto i;
#ifdef NO_LOCAL_LABELS
  extrn esn, isn1st;
#endif
#ifdef FLAG_UNDEF
  extrn symtab, ns;
  auto rp;
#endif

#ifdef NO_LOCAL_LABELS
  if (isn == isn1st)
    return;
  i = isn1st;
#else
  if (!isn)
    return;
  write('1:');   /* XXX local label */
  i = 0;
#endif
dmpst();
  while (i < isn) {
    write('L');
    number(i);
    write(':l');
    number(i);
    writenl();
    i = i+1;
  }
#ifdef NO_LOCAL_LABELS
  isn1st = isn;
#else
  isn = 0;
#endif

#ifdef FLAG_UNDEF
  /* search for undefined symbols . */
  rp = symtab+SYMTAB_NEXT;
  while (rp < ns) {
    if ((*rp) == CL_INTERNAL)
      errornm ('un', rp+2);
    rp = rp+2;  /* move up to the name */
    while (*rp) /* move to the end of the name */
      rp = rp+1;
    rp = rp+1; /* move up to the start of the next entry */
  }
#endif
}

/**********
 *
 * gen
 *
 */

gen(o,n) {
  write(o);
  write(' ');
  number(n);
  writenl();
}

/**********
 *
 * jumpc
 *
 */

jumpc(n) {
#ifdef NO_LOCAL_LABELS
  extrn esn, isn1st;
#endif
  write('f '); /* ifop */
#ifdef NO_LOCAL_LABELS
  write('L');
  number(n);
#else
  write('1f');   /* XXX local label */
  write('+');
  number(n);
#endif
  writenl();
}

/**********
 *
 * jump
 *
 */

jump(n) {
#ifdef NO_LOCAL_LABELS
  extrn esn, isn1st;
#endif
  write('x ');
#ifdef NO_LOCAL_LABELS
  write('L');
  number(n);
#else
  write('1f');   /* XXX local label */
  write('+');
  number(n);
#endif
#if 0 /* too clever */
  gen('*nn',6); /* goto */
#else
  writenl();
  gen_goto();
#endif
}

/**********
 *
 * label
 *
 */

label(n) {
  write('l');
  number(n);
  write('=.');
  writenl();
}

#ifdef NO_LOCAL_LABELS

/**********
 *
 * glabel
 *
 */

glabel(n) {
  write('L');
  number(n);
  write('=.');
  writenl();
}
#endif

/**********
 *
 * printn
 *
 */

printn(n) {
  if (n > 9) {
    printn(n / 10);
    n = n % 10;
  }
  write(n + '0');
}

/**********
 *
 * number
 *
 */

number(x) {
  if (x < 0) {
    write('-');
    x = -x;
  }
  printn(x);
}

/**********
 *
 * name
 *
 */

name(s) {
  while (*s) {
    write(*s);
    s = s+1;
  }
}

/**********
 *
 * errornm
 *
 */

errornm(code, p) {
  extrn line, eof, csym, nerror, fout;
  auto f;

  if (eof | nerror==20) {
    eof = 1;
    return;
  }
  nerror = nerror+1;
  flush();
  f = fout;
  fout = 1;
  write(code);
  write(' ');
  name(p);
  write(' ');
  printn(line);
  writenl();
  flush();
  fout = f;
}

/**********
 *
 * error
 *
 */

error(code) {
  extrn line, eof, csym, nerror, fout;
  auto f;

  if (eof | nerror==20) {
    eof = 1;
    return;
  }
  nerror = nerror+1;
  flush();
  f = fout;
  fout = 1;
  write(code);
  write(' ');
  printn(line);
  writenl();
  flush();
  fout = f;
}

