#!/bin/bash

# Build B compiler
#
# 1. Bootstrap DPDP7 B compiler to UNIX/C
#
#    pdp7-unix is https://github.com/DoctorWkt/pdp7-unix
#
#    pdp7-unix/tools/b.c     pdp7 B Compiler in C
#    pdp7-unix/src/other/b.b pdp7 B Compiler in B
#

# a7out needs pdp7-unix file names, so make a local copy of b.b

cp ../pdp7-unix-stash/b.b .

# Get the pdp7 B compiler in C
# Patch to clean warnings
# Build as tools_b

cp ../pdp7-unix-stash/b.c tools_b.c
patch < tools_b.c.patch
clang -Wno-multichar tools_b.c -o tools_b

# Build the pdp7 B compiler in B with tools_b to b_tools.s
# Assemble the pdp7 B compiler to b_tools.out

./tools_b b.b b_tools_b.s
perl ../pdp7-unix-stash/as7 --out b_tools_b.out \
                             ../pdp7-unix-stash/bl.s \
                             b_tools_b.s \
                             ../pdp7-unix-stash/bi.s

# Build the pdp7 B compiler with the pdp7 B compiler to b2.s
# Compare b_tools.s to b2.s; if they are the same, then the B compiler built
# with the C compiler works the same as the B compiler built with pdp7 B.


perl ../pdp7-unix-stash/a7out b_tools_b.out b.b b2.s
diff b_tools_b.s b2.s -s && echo Success

# Extra credit: Assemble b2.s and compare b_tools_b.out and b2.out

#perl ../pdp7-unix/tools/as7 --out b2.out ../pdp7-unix-stash/bl.s b2.s ../pdp7-unix-stash/bi.s
#diff b_tools_b.out b2.out -s


	

